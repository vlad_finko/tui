variable "ami_id" {
  default = "ami-023c8dbf8268fb3ca"
}

variable "instance_type_webserver" {
  default = "t2.micro"
}

variable "instance_type_database" {
  default = "t2.micro"
}

variable "cidr_block" {
  default = "172.16.0.0/16"
}

variable "cidr_block_public_subnet" {
  default = "172.16.1.0/24"
}

variable "cidr_block_private_subnet" {
  default = "172.16.2.0/24"
}

variable "ports_webserver" {
  type    = list(string)
  default = ["80", "22"]
}

variable "ports_database" {
  type    = list(string)
  default = ["3110", "22"]
}

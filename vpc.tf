resource "aws_vpc" "vpc" {
  cidr_block       = var.cidr_block
  instance_tenancy = "default"

  tags = {
    Creator = "Vladyslav Finko"
    Name    = "awslab-vpc"
  }
}

resource "aws_internet_gateway" "internet-gateway" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Creator = "Vladyslav Finko"
    Name    = "internet-gateway"
  }
}

resource "aws_subnet" "public-subnet" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cidr_block_public_subnet
  availability_zone_id    = "use2-az1"
  map_public_ip_on_launch = true

  tags = {
    Creator = "Vladyslav Finko"
    Name    = "awslab-subnet-public"
  }
}

resource "aws_subnet" "private-subnet" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cidr_block_private_subnet
  availability_zone_id    = "use2-az1"

  tags = {
    Creator = "Vladyslav Finko"
    Name    = "awslab-subnet-private"
  }
}

resource "aws_route_table" "public-route-table" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet-gateway.id
  }

  tags = {
    Creator = "Vladyslav Finko"
    Name    = "awslab-rt-internet"
  }
}

resource "aws_route_table_association" "public-subnet-route-table-association" {
  subnet_id      = aws_subnet.public-subnet.id
  route_table_id = aws_route_table.public-route-table.id
}

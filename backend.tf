terraform {
  backend "s3" {
    bucket = "devopsacademy-terraform-state-newe"
    key    = "terraform.state"
    region = "us-east-2"
  }
}

provider "aws" {
  region = "us-east-2"
}

resource "aws_instance" "webserver" {
  ami                    = var.ami_id
  instance_type          = var.instance_type_webserver
  vpc_security_group_ids = [aws_security_group.webserver-security-group.id]
  subnet_id              = aws_subnet.public-subnet.id

  tags = {
    Creator = "Vladyslav Finko"
    Name    = "webserver"
  }
}

resource "aws_security_group" "webserver-security-group" {
  name        = "webserver-security-group"
  description = "webserver-security-group"
  vpc_id      = aws_vpc.vpc.id

  dynamic "ingress" {
    for_each = var.ports_webserver
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  ingress {
    description      = "TLS from VPC"
    from_port        = 0
    to_port          = 0
    protocol         = "icmp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port    = 0
    to_port      = 0
    protocol     = "-1"
    cidr_blocks  = ["0.0.0.0/0"]
  }

  tags = {
    Creator = "Vladyslav Finko"
    Name    = "webserver-security_group"
  }
}

resource "aws_instance" "database" {
  ami                    = var.ami_id
  instance_type          = var.instance_type_database
  vpc_security_group_ids = [aws_security_group.database-security-group.id]
  subnet_id              = aws_subnet.private-subnet.id

  tags = {
    Creator = "Vladyslav Finko"
    Name    = "database"
  }
}

resource "aws_security_group" "database-security-group" {
  name        = "database-security-group"
  description = "database-security-group"
  vpc_id      = aws_vpc.vpc.id

  dynamic "ingress" {
    for_each = var.ports_database
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["172.16.1.0/24"]
    }
  }

  ingress {
    description      = "ICMP"
    from_port        = 0
    to_port          = 0
    protocol         = "icmp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port    = 0
    to_port      = 0
    protocol     = "-1"
    cidr_blocks  = ["0.0.0.0/0"]
  }

  tags = {
    Creator = "Vladyslav Finko"
    Name    = "database-security_group"
  }
}

resource "aws_instance" "webserver" {
  ami                    = var.ami_id
  instance_type          = var.instance_type_webserver
  vpc_security_group_ids = [aws_security_group.webserver-security-group.id]
  subnet_id              = aws_subnet.public-subnet.id

  tags = {
    Creator = "Vladyslav Finko"
    Name    = "webserver"
  }
}

resource "aws_security_group" "webserver-security-group" {
  name        = "webserver-security-group"
  description = "webserver-security-group"
  vpc_id      = aws_vpc.vpc.id

  dynamic "ingress" {
    for_each = var.ports_webserver
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  ingress {
    description      = "TLS from VPC"
    from_port        = 0
    to_port          = 0
    protocol         = "icmp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port    = 0
    to_port      = 0
    protocol     = "-1"
    cidr_blocks  = ["0.0.0.0/0"]
  }

  tags = {
    Creator = "Vladyslav Finko"
    Name    = "webserver-security_group"
  }
}

resource "aws_instance" "database" {
  ami                    = var.ami_id
  instance_type          = var.instance_type_database
  vpc_security_group_ids = [aws_security_group.database-security-group.id]
  subnet_id              = aws_subnet.private-subnet.id

  tags = {
    Creator = "Vladyslav Finko"
    Name    = "database"
  }
}

resource "aws_security_group" "database-security-group" {
  name        = "database-security-group"
  description = "database-security-group"
  vpc_id      = aws_vpc.vpc.id

  dynamic "ingress" {
    for_each = var.ports_database
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["172.16.1.0/24"]
    }
  }

  ingress {
    description      = "ICMP"
    from_port        = 0
    to_port          = 0
    protocol         = "icmp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port    = 0
    to_port      = 0
    protocol     = "-1"
    cidr_blocks  = ["0.0.0.0/0"]
  }

  tags = {
    Creator = "Vladyslav Finko"
    Name    = "database-security_group"
  }
}
}

